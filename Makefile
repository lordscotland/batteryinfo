ARCHS = arm64

include theos/makefiles/common.mk

TWEAK_NAME = BatteryInfo
BatteryInfo_FILES = Tweak.m
BatteryInfo_FRAMEWORKS = CoreGraphics UIKit
BatteryInfo_PRIVATE_FRAMEWORKS = IOKit Preferences

include $(THEOS_MAKE_PATH)/tweak.mk
