#import <Preferences/Preferences.h>
#include <substrate.h>

static NSString* const kBCMaximumCapacityKey=@"rMC";
static NSString* const kBCCurrentCapacityKey=@"rCC";
static NSString* const kBCInstantAmperageKey=@"rIA";

@interface UIColor (UIColorSystemColors_PendingAPI)
+(instancetype)systemDarkGreenColor;
+(instancetype)systemDarkOrangeColor;
+(instancetype)systemDarkRedColor;
+(instancetype)systemYellowColor;
@end

@interface PSTableCell : UITableViewCell
-(void)setIcon:(UIImage*)icon;
@end

@interface BatteryChartCell : PSTableCell @end
@implementation BatteryChartCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier {
  if((self=[super initWithStyle:style reuseIdentifier:reuseIdentifier])){
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    self.textLabel.numberOfLines=0;
  }
  return self;
}
-(void)refreshCellContentsWithSpecifier:(PSSpecifier*)specifier {
  CGFloat vH;
  CFNumberRef oH=(CFNumberRef)[specifier propertyForKey:PSTableCellHeightKey];
  if(!oH || !CFNumberGetValue(oH,kCFNumberCGFloatType,&vH)){vH=self.bounds.size.height;}
  const float rMC=((NSNumber*)[specifier propertyForKey:kBCMaximumCapacityKey]).floatValue;
  const float rCC=((NSNumber*)[specifier propertyForKey:kBCCurrentCapacityKey]).floatValue;
  const float rIA=((NSNumber*)[specifier propertyForKey:kBCInstantAmperageKey]).floatValue;
  const float rr=rMC?rCC/rMC:0;
  const CGFloat scale=2,pmargin=10,rfratio=0.8,a0=M_PI/2;
  const CGFloat psize=(vH-2*pmargin)*scale,midx=(psize-1)/2,rwidth=midx/3,rfwidth=rwidth*rfratio;
  UIColor* C_rMC=[UIColor systemDarkRedColor];
  UIColor* C_rCC=[UIColor systemDarkOrangeColor];
  UIColor* C_rIA=[UIColor systemYellowColor];
  UIColor* C_rr=[UIColor systemDarkGreenColor];
  CGColorSpaceRef cspace=CGColorSpaceCreateDeviceRGB();
  CGContextRef context=CGBitmapContextCreate(NULL,psize,psize,
   8,0,cspace,(CGBitmapInfo)kCGImageAlphaPremultipliedLast);
  CGColorSpaceRelease(cspace);
  if(rMC){
    const CGFloat r0=midx,r1=r0-rfwidth,a=a0-rMC*2*M_PI;
    CGContextAddArc(context,midx,midx,r0,a0,a,false);
    CGContextAddArc(context,midx,midx,r1,a,a0,true);
    CGContextSetRGBFillColor(context,0.9,0.9,0.9,1);
    CGContextFillPath(context);
    CGContextAddArc(context,midx,midx,r0,a0,a,true);
    CGContextAddArc(context,midx,midx,r1,a,a0,false);
    CGContextSetFillColorWithColor(context,C_rMC.CGColor);
    CGContextFillPath(context);
  }
  if(rCC){
    const CGFloat r0=midx-rwidth,r1=r0-rfwidth,a=a0-rCC*2*M_PI;
    CGContextAddArc(context,midx,midx,r0,a0,a,true);
    CGContextAddArc(context,midx,midx,r1,a,a0,false);
    CGContextSetFillColorWithColor(context,C_rCC.CGColor);
    CGContextFillPath(context);
    if(rIA){
      const bool charging=(rIA>0);
      const CGFloat ax=a-rIA*2*M_PI;
      CGContextAddArc(context,midx,midx,charging?r0:r0-rfwidth/2,a,ax,charging);
      CGContextAddArc(context,midx,midx,r1,ax,a,!charging);
      CGContextSetFillColorWithColor(context,C_rIA.CGColor);
      CGContextFillPath(context);
    }
  }
  if(rr){
    CGContextMoveToPoint(context,midx,midx);
    CGContextAddArc(context,midx,midx,midx-rwidth*2,a0,a0-rr*2*M_PI,true);
    CGContextSetFillColorWithColor(context,C_rr.CGColor);
    CGContextFillPath(context);
  }
  CGImageRef img=CGBitmapContextCreateImage(context);
  self.icon=[UIImage imageWithCGImage:img scale:scale orientation:UIImageOrientationUp];
  CGImageRelease(img);
  CGContextRelease(context);
  NSString* part;
  NSMutableString* string=[[NSMutableString alloc] initWithString:@"Capacity: "];
  NSRange R_rMC=NSMakeRange(string.length,(part=[NSString
   localizedStringWithFormat:@"%.1f%%",rMC*100]).length);
  [string appendString:part];
  [string appendString:@"\nCharge: "];
  NSRange R_rCC=NSMakeRange(string.length,(part=[NSString
   localizedStringWithFormat:@"%.1f%%",rCC*100]).length);
  [string appendString:part];
  [string appendString:@" ("];
  NSRange R_rIA=NSMakeRange(string.length,(part=rIA?[NSString
   localizedStringWithFormat:@"%+.1f%%/h",rIA*100]:@"-full-").length);
  [string appendString:part];
  [string appendString:@")\nPercentage: "];
  NSRange R_rr=NSMakeRange(string.length,(part=[NSString
   localizedStringWithFormat:@"%.1f%%",rr*100]).length);
  [string appendString:part];
  NSMutableAttributedString* astring=[[NSMutableAttributedString alloc]
   initWithString:string attributes:nil];
  [string release];
  [astring beginEditing];
  [astring setAttributes:@{NSForegroundColorAttributeName:C_rMC} range:R_rMC];
  [astring setAttributes:@{NSForegroundColorAttributeName:C_rCC} range:R_rCC];
  [astring setAttributes:@{NSForegroundColorAttributeName:C_rIA} range:R_rIA];
  [astring setAttributes:@{NSForegroundColorAttributeName:C_rr} range:R_rr];
  [astring endEditing];
  [self.textLabel.attributedText=astring release];
}
@end

@interface BatteryInfoController : PSListController @end
@implementation BatteryInfoController
-(NSString*)valueForSpecifier:(PSSpecifier*)specifier {
  return [specifier propertyForKey:PSValueKey];
}
-(PSSpecifier*)specifierWithTitle:(NSString*)title value:(NSString*)value {
  PSSpecifier* specifier=[PSSpecifier preferenceSpecifierNamed:title target:self
   set:NULL get:@selector(valueForSpecifier:) detail:nil cell:PSTitleValueCell edit:nil];
  [specifier setProperty:value forKey:PSValueKey];
  return specifier;
}
-(NSArray*)specifiers {
  if(_specifiers){return _specifiers;}
  NSMutableArray* specifiers=[[NSMutableArray alloc] init];
  extern mach_port_t kIOMasterPortDefault;
  extern CFMutableDictionaryRef IOServiceMatching(const char*);
  extern mach_port_t IOServiceGetMatchingService(mach_port_t,CFDictionaryRef);
  extern kern_return_t IORegistryEntryCreateCFProperties(mach_port_t,CFDictionaryRef*,CFAllocatorRef,UInt32);
  extern kern_return_t IOObjectRelease(mach_port_t);
  mach_port_t entry=IOServiceGetMatchingService(kIOMasterPortDefault,
   IOServiceMatching("IOPMPowerSource"));
  if(entry){
    CFDictionaryRef dict;
    if(IORegistryEntryCreateCFProperties(entry,&dict,NULL,0)==0){
      int value,vCC,vMC,vDC,vIA;
      CFNumberRef oValue,oCC,oMC,oDC,oIA;
      if((oCC=CFDictionaryGetValue(dict,CFSTR("AppleRawCurrentCapacity")))
       && !CFNumberGetValue(oCC,kCFNumberIntType,&vCC)){oCC=NULL;}
      if((oMC=CFDictionaryGetValue(dict,CFSTR("AppleRawMaxCapacity")))
       && !CFNumberGetValue(oMC,kCFNumberIntType,&vMC)){oMC=NULL;}
      if((oDC=CFDictionaryGetValue(dict,CFSTR("DesignCapacity")))
       && !CFNumberGetValue(oDC,kCFNumberIntType,&vDC)){oDC=NULL;}
      if((oIA=CFDictionaryGetValue(dict,CFSTR("InstantAmperage")))
       && !CFNumberGetValue(oIA,kCFNumberIntType,&vIA)){oIA=NULL;}
      if(oDC){
        [specifiers addObject:[PSSpecifier emptyGroupSpecifier]];
        PSSpecifier* specifier=[PSSpecifier preferenceSpecifierNamed:nil target:nil
         set:NULL get:NULL detail:nil cell:PSDefaultCell edit:nil];
        [specifier setProperty:[BatteryChartCell class] forKey:PSCellClassKey];
        [specifier setProperty:@(100) forKey:PSTableCellHeightKey];
        if(oMC){[specifier setProperty:@((float)vMC/vDC) forKey:kBCMaximumCapacityKey];}
        if(oCC){[specifier setProperty:@((float)vCC/vDC) forKey:kBCCurrentCapacityKey];}
        if(oIA){[specifier setProperty:@((float)vIA/vDC) forKey:kBCInstantAmperageKey];}
        [specifiers addObject:specifier];
      }
      if(oIA){
        [specifiers addObject:[self specifierWithTitle:@"InstantAmperage"
         value:[NSString localizedStringWithFormat:@"%+d mA",vIA]]];
      }
      if(oCC){
        [specifiers addObject:[self specifierWithTitle:@"AppleRawCurrentCapacity"
         value:[NSString localizedStringWithFormat:@"%d mAh",vCC]]];
      }
      if(oMC){
        [specifiers addObject:[self specifierWithTitle:@"AppleRawMaxCapacity"
         value:[NSString localizedStringWithFormat:@"%d mAh",vMC]]];
      }
      if(oDC){
        [specifiers addObject:[self specifierWithTitle:@"DesignCapacity"
         value:[NSString localizedStringWithFormat:@"%d mAh",vDC]]];
      }
      PSSpecifier* specifier=[PSSpecifier emptyGroupSpecifier];
      if((oValue=CFDictionaryGetValue(dict,CFSTR("UpdateTime")))
       && CFNumberGetValue(oValue,kCFNumberIntType,&value)){
        [specifier setProperty:[@"Last updated: " stringByAppendingString:[NSDateFormatter
         localizedStringFromDate:[NSDate dateWithTimeIntervalSince1970:value]
         dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterMediumStyle]]
         forKey:PSFooterTextGroupKey];
      }
      [specifiers addObject:specifier];
      if((oValue=CFDictionaryGetValue(dict,CFSTR("Temperature")))
       && CFNumberGetValue(oValue,kCFNumberIntType,&value)){
        const float celsius=(float)value/100;
        NSString* title=@"Temperature";
        if(celsius<0 || celsius>35){title=[@"\u2757" stringByAppendingString:title];}
        [specifiers addObject:[self specifierWithTitle:title
         value:[NSString localizedStringWithFormat:@"%.2f \u2103",celsius]]];
      }
      if((oValue=CFDictionaryGetValue(dict,CFSTR("CycleCount")))
       && CFNumberGetValue(oValue,kCFNumberIntType,&value)){
        [specifiers addObject:[self specifierWithTitle:@"CycleCount"
         value:[NSString localizedStringWithFormat:@"%d",value]]];
      }
      if((oValue=CFDictionaryGetValue(dict,CFSTR("Amperage")))
       && CFNumberGetValue(oValue,kCFNumberIntType,&value)){
        [specifiers addObject:[self specifierWithTitle:@"Amperage"
         value:[NSString localizedStringWithFormat:@"%d mA",value]]];
      }
      if((oValue=CFDictionaryGetValue(dict,CFSTR("Voltage")))
       && CFNumberGetValue(oValue,kCFNumberIntType,&value)){
        [specifiers addObject:[self specifierWithTitle:@"Voltage"
         value:[NSString localizedStringWithFormat:@"%.3f V",(float)value/1000]]];
      }
      CFRelease(dict);
    }
    IOObjectRelease(entry);
  }
  return _specifiers=specifiers;
}
-(void)reloadUI:(UIRefreshControl*)refresh {
  [refresh endRefreshing];
  [self reloadSpecifiers];
}
-(void)loadView {
  [super loadView];
  UIRefreshControl* refresh=[[UIRefreshControl alloc] init];
  [refresh addTarget:self action:@selector(reloadUI:)
   forControlEvents:UIControlEventValueChanged];
  [_table addSubview:refresh];
  [refresh release];
}
@end

static NSMutableArray* (*F_loadSpecifiers)(PSListController*,SEL,NSString*,id);
static NSMutableArray* f_loadSpecifiers(PSListController* self,SEL _cmd,NSString* plistName,id target) {
  NSMutableArray* specifiers=F_loadSpecifiers(self,_cmd,plistName,target);
  if(strcmp(object_getClassName(target),"UsageController")==0){
    PSSpecifier* specifier=[PSSpecifier preferenceSpecifierNamed:@"Battery Information" target:self
     set:NULL get:NULL detail:[BatteryInfoController class] cell:PSLinkCell edit:nil];
    [specifier setProperty:@"BATTERY_INFO" forKey:PSIDKey];
    [specifiers addObject:specifier];
  }
  return specifiers;
}

static __attribute__((constructor)) void init() {
  MSHookMessageEx([PSListController class],@selector(loadSpecifiersFromPlistName:target:),
   (IMP)f_loadSpecifiers,(IMP*)&F_loadSpecifiers);
}
